# Semantic Versioning parser aka SemVer 2.0.0
 
![Build status](https://bitbucket.org/dedipresta/semver/addon/pipelines/home#!/badge/branch/master?svg=true "Build status")

## How to use

### SemVer, SimpleRange, AbstractRange

#### Parse a string
```scala
SemVer.parse("1.2.3-rc1+build123456")
```

#### Instantiate a SemVer
```scala
SemVer(major = 1, minor = 2, patch = 3)
SemVer(1,2,3,Seq("rc1"),Seq("build123456", "v1"))
```
    
#### Add other info
```scala
SemVer(1,2,3).witPreRelease("rc1.1").withMetaData("build123456.v1")
```

### Some functions
```scala
SemVer.isValid("1.2.3")
SemVer.isInvalid("1.2.3")
SemVer(1,2,3).incrementMajor
SemVer(1,2,3).incrementMinor
SemVer(1,2,3).incrementPatch
SemVer(1,2,3).isStable
SemVer(1,2,3).isUnstable
SemVer(1,2,3).hasMetaData
```

#### Compare SemVer
```scala
SemVer(1,2,3) < SemVer(1,2,4)
You may use <, <=, ==, >= >
```
**== is exact match** different build info will result in different comparison

#### Check requirements
```scala
SemVer(1,2,3).satisfies(SimpleRange(from = SemVer(1,0,0), to = SemVer(1,5,9)))
SemVer(1,2,3).satisfies(AbstractRange.parse("1.x").get)
```

```scala
SemVer.minSatisfy(choices = Seq(SemVer(1,2,3), ranges = Seq(AbstractRange.parse("1.x").get))
SemVer.maxSatisfy(choices = Seq(SemVer(1,2,3), ranges = Seq(AbstractRange.parse("1.x").get))
```


## Tests
Run tests and coverage with **sbt**

    1. sbt clean coverage test
    2. sbt coverageReport