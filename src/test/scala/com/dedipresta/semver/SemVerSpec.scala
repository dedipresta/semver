package com.dedipresta.semver

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner


@RunWith(classOf[JUnitRunner])
object SemVerSpec extends Specification {

  val semVer123 = SemVer(1, 2, 3)

  "SemVer" should {
    "parse valid string 0.0.0" in {
      SemVer.parse("0.0.0") must beSome(SemVer(0, 0, 0))
    }
    "parse valid string 1.0.0" in {
      SemVer.parse("1.0.0") must beSome(SemVer(1, 0, 0))
    }
    "parse valid string 1.2.3" in {
      SemVer.parse("1.2.3") must beSome(SemVer(1, 2, 3))
    }
    "parse valid string 42.84.168" in {
      SemVer.parse("42.84.168") must beSome(SemVer(42, 84, 168))
    }
    "parse valid string with pre-release info" in {
      SemVer.parse("1.2.3-rc1") must beSome(SemVer(1, 2, 3, List("rc1")))
    }
    "parse valid string with multiple pre-release info" in {
      SemVer.parse("1.2.3-rc1.extra-info.1") must beSome(SemVer(1, 2, 3, List("rc1", "extra-info", "1")))
    }
    "parse valid string with meta-data info" in {
      SemVer.parse("1.2.3+build1234") must beSome(SemVer(1, 2, 3, metaData = List("build1234")))
    }
    "parse valid string with multiple meta-data info" in {
      SemVer.parse("1.2.3+build1234.extra-info.1") must beSome(SemVer(1, 2, 3, metaData = List("build1234", "extra-info", "1")))
    }
    "parse valid string with multiple pre-release and meta-data info" in {
      SemVer.parse("1.2.3-rc1.extra.1+build1234.extra-info.1") must beSome(SemVer(1, 2, 3, List("rc1", "extra", "1"), List("build1234", "extra-info", "1")))
    }

    "not parse invalid string 0.0" in {
      SemVer.parse("0.0") must beNone
    }
    "not parse invalid string 1.0" in {
      SemVer.parse("1.0") must beNone
    }
    "not parse invalid string 1" in {
      SemVer.parse("1") must beNone
    }
    "not parse invalid string 1.x" in {
      SemVer.parse("1.x") must beNone
    }
    "not parse invalid string with negative -1.1.0" in {
      SemVer.parse("-1.1.0") must beNone
    }
    "not parse invalid string with negative 1.-1.0" in {
      SemVer.parse("1.-1.0") must beNone
    }
    "not parse invalid string with negative 1.1.-1" in {
      SemVer.parse("1.1.-1") must beNone
    }
    "not parse invalid string with leading 01.0.0" in {
      SemVer.parse("01.0.0") must beNone
    }
    "not parse invalid string with leading 0.01.0" in {
      SemVer.parse("0.01.0") must beNone
    }
    "not parse invalid string with leading 0.0.01" in {
      SemVer.parse("0.0.01") must beNone
    }

    "not parse string with invalid meta-data info character %" in {
      SemVer.parse("1.2.3-r%c1") must beNone
    }
    "not parse string with invalid meta-data info character _" in {
      SemVer.parse("1.2.3-r_c1") must beNone
    }
    "not parse string with invalid pre-release info character %" in {
      SemVer.parse("1.2.3+extra%meta") must beNone
    }
    "not parse string with invalid pre-release info character _" in {
      SemVer.parse("1.2.3+extra_meta") must beNone
    }

    "check validity of valid version and return true" in {
      SemVer.isValid("1.2.3") must beTrue
    }
    "check validity of invalid version and return false" in {
      SemVer.isValid("1.2.3.4") must beFalse
    }
    "check invalidity of valid version and return false" in {
      SemVer.isInvalid("1.2.3") must beFalse
    }
    "check invalidity of invalid version and return true" in {
      SemVer.isInvalid("1.2.3.4") must beTrue
    }

    "allow to creation without argument" in {
      SemVer() must beEqualTo(SemVer(0, 0, 0))
    }

    "allow access to field major" in {
      semVer123.major must beEqualTo(1)
    }
    "allow access to field minor" in {
      semVer123.minor must beEqualTo(2)
    }
    "allow access to field patch" in {
      semVer123.patch must beEqualTo(3)
    }

    "allow to increment major version" in {
      semVer123.incrementMajor must beEqualTo(SemVer(2, 0, 0))
    }
    "allow to increment minor version" in {
      semVer123.incrementMinor must beEqualTo(SemVer(1, 3, 0))
    }
    "allow to increment patch version" in {
      semVer123.incrementPatch must beEqualTo(SemVer(1, 2, 4))
    }

    "allow to add multiple pre-release info" in {
      semVer123.withPreRelease("rc1.extra-info.1") must beEqualTo(SemVer(1, 2, 3, List("rc1", "extra-info", "1")))
    }
    "allow to add multiple pre-release info as seg" in {
      semVer123.withPreRelease(Seq("rc1","extra-info", "1")) must beEqualTo(SemVer(1, 2, 3, List("rc1", "extra-info", "1")))
    }
    "allow to add multiple meta-data info" in {
      semVer123.withMetaData("build1234.extra-info.1") must beEqualTo(SemVer(1, 2, 3, Nil, List("build1234", "extra-info", "1")))
    }
    "allow to add multiple meta-data info as seq" in {
      semVer123.withMetaData(Seq("build1234", "extra-info", "1")) must beEqualTo(SemVer(1, 2, 3, Nil, List("build1234", "extra-info", "1")))
    }

    "throw an exception when adding invalid pre-release info" in {
      semVer123.withPreRelease("rc1.extra_info.1") must throwA[IllegalArgumentException]
    }
    "throw an exception when adding invalid pre-release info as seq" in {
      semVer123.withPreRelease(Seq("rc1", "extra_info", "1")) must throwA[IllegalArgumentException]
    }
    "throw an exception when adding invalid meta-data info" in {
      semVer123.withMetaData("build1234.extra_info.1") must throwA[IllegalArgumentException]
    }
    "throw an exception when adding invalid meta-data info as seq" in {
      semVer123.withMetaData(Seq("build1234", "extra_info", "1")) must throwA[IllegalArgumentException]
    }

    "not to be tagged as pre-release when pre-release version is not set" in {
      semVer123.isPreRelease must beFalse
    }
    "to be tagged as pre-release when pre-release version is set" in {
      semVer123.withPreRelease("rc1.extra-info.1").isPreRelease must beTrue
    }
    "not to be tagged as unstable when pre-release version is not set" in {
      semVer123.isUnstable must beFalse
      semVer123.isStable must beTrue
    }
    "to be tagged as unstable when pre-release version is set" in {
      semVer123.withPreRelease("rc1.extra-info.1").isUnstable must beTrue
      semVer123.withPreRelease("rc1.extra-info.1").isStable must beFalse
    }
    "to be check if meta-data are set" in {
      semVer123.withMetaData("build1234.extra-info.1").hasMetaData must beTrue
      semVer123.withMetaData(Seq()).hasMetaData must beFalse
      semVer123.hasMetaData must beFalse
    }

    "allow to compare numerically the same version" in {
      semVer123 compareTo semVer123 must beEqualTo(0)
    }
    "allow to compare numerically lower version" in {
      semVer123 compareTo SemVer(1,2,4) must beEqualTo(-1)
    }
    "allow to compare numerically greater version" in {
      SemVer(1,2,4) compareTo semVer123 must beEqualTo(1)
    }

    "allow to compare with == the same version" in {
      semVer123 == semVer123 must beTrue
    }
    "allow to compare with <= the same version" in {
      semVer123 <= semVer123 must beTrue
    }
    "allow to compare with >= the same version" in {
      semVer123 >= semVer123 must beTrue
    }
    "allow to compare with < lower version" in {
      semVer123 < SemVer(1,2,4) must beTrue
      SemVer(1,2,4) < semVer123 must beFalse
    }
    "allow to compare with >= greater version" in {
      semVer123 >= SemVer(1,2,4) must beFalse
      SemVer(1,2,4) >= semVer123 must beTrue
    }
    "allow to compare with > greater version" in {
      semVer123 > SemVer(1,2,4) must beFalse
      SemVer(1,2,4) > semVer123 must beTrue
    }

    "check when version satisfies a simple range reduced to itself" in {
      semVer123.satisfies(SimpleRange(from = semVer123, to = semVer123)) must beTrue
    }
    "check when version satisfies a simple range" in {
      semVer123.satisfies(SimpleRange(from = SemVer(1,0,0), to = SemVer(1,2,4))) must beTrue
    }
    "check when version satisfies a simple range starting with the same version" in {
      semVer123.satisfies(SimpleRange(from = semVer123, to = SemVer(1,2,4))) must beTrue
    }
    "check when version satisfies a simple range ending with the same version" in {
      semVer123.satisfies(SimpleRange(from = SemVer(1,0,0), to = semVer123)) must beTrue
    }
    "check when version does not satisfy a simple range" in {
      semVer123.satisfies(SimpleRange(from = SemVer(1,2,4), to = SemVer(2,0,0))) must beFalse
    }

    "check when version satisfies an abstract range reduced to itself" in {
      AbstractRange.parse(semVer123.toString).map(semVer123.satisfies(_)) must beSome(true)
    }
    "check when version satisfies an abstract range on patch" in {
      AbstractRange.parse("1.2.x").map(semVer123.satisfies(_)) must beSome(true)
    }
    "check when version satisfies an abstract range on minor" in {
      AbstractRange.parse("1.x.3").map(semVer123.satisfies(_)) must beSome(true)
    }
    "check when version satisfies an abstract range on major" in {
      AbstractRange.parse("x.2.3").map(semVer123.satisfies(_)) must beSome(true)
    }
    "check when version does not satisfy an abstract range on major" in {
      AbstractRange.parse("2.2.3").map(semVer123.satisfies(_)) must beSome(false)
    }

    "check when version cannot satisfy an empty set of ranges" in {
      semVer123.satisfies(ranges = Set.empty[SemVerRange]) must beFalse
    }
    "check when version satisfy a range from a set of ranges" in {
      semVer123.satisfies(ranges = Set(
        "1.3.x",
        "1.2.3",
        "4.5.6"
      ).flatMap(AbstractRange.parse).map(_.asInstanceOf[SemVerRange])) must beTrue
    }

    "allow to retrieve the minimum SemVer satisfying ranges" in {
      SemVer.minSatisfy(
        choices = Set(SemVer(1,5,8), SemVer(1,3,2), SemVer(1,3,1), SemVer(2,2,2)),
        ranges = Set("1.3.x", "1.2.3", "4.5.6")
          .flatMap(AbstractRange.parse)
          .map(_.asInstanceOf[SemVerRange]
        )) must beSome(SemVer(1,3,1))
    }
    "return None when no minimum SemVer satisfies set of ranges" in {
      SemVer.minSatisfy(
        choices = Set(SemVer(1,5,8), SemVer(1,4,2), SemVer(1,4,1), SemVer(2,2,2)),
        ranges = Set("1.3.x", "1.2.3", "4.5.6")
          .flatMap(AbstractRange.parse)
          .map(_.asInstanceOf[SemVerRange]
        )) must beNone
    }

    "allow to retrieve the maximum SemVer satisfying ranges" in {
      SemVer.maxSatisfy(
        choices = Set(SemVer(1,5,8), SemVer(1,3,2), SemVer(1,3,1), SemVer(2,2,2)),
        ranges = Set("1.3.x", "1.2.3", "4.5.6")
          .flatMap(AbstractRange.parse)
          .map(_.asInstanceOf[SemVerRange]
        )) must beSome(SemVer(1,3,2))
    }
    "return None when no maximum SemVer satisfies set of ranges" in {
      SemVer.maxSatisfy(
        choices = Set(SemVer(1,5,8), SemVer(1,4,2), SemVer(1,4,1), SemVer(2,2,2)),
        ranges = Set("1.3.x", "1.2.3", "4.5.6")
          .flatMap(AbstractRange.parse)
          .map(_.asInstanceOf[SemVerRange]
        )) must beNone
    }
  }
}