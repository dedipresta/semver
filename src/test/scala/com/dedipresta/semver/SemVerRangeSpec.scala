package com.dedipresta.semver

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner


@RunWith(classOf[JUnitRunner])
object SemVerRangeSpec extends Specification {

  val semVer123 = SemVer(1, 2, 3)

  "SemVerRange" should {
    "have an abstract value x" in {
      AbstractRange.AbstractValue == 'x'
    }
    "have a default value that is fuly abstract" in {
      AbstractRange() must beEqualTo(AbstractRange(Left(AbstractRange.AbstractValue), Left(AbstractRange.AbstractValue), Left(AbstractRange.AbstractValue)))
    }

    "allow to parse abstract range from valid string 0.0.0" in {
      AbstractRange.parse("0.0.0") must beSome(AbstractRange(Right(0), Right(0), Right(0)))
    }
    "allow to parse abstract range from valid string 1.2.5" in {
      AbstractRange.parse("1.2.5") must beSome(AbstractRange(Right(1), Right(2), Right(5)))
    }
    "allow to parse abstract range from valid string x" in {
      AbstractRange.parse("x") must beSome(AbstractRange(Left(AbstractRange.AbstractValue), Left(AbstractRange.AbstractValue), Left(AbstractRange.AbstractValue)))
    }
    "allow to parse abstract range from valid string 1.x" in {
      AbstractRange.parse("1.x") must beSome(AbstractRange(Right(1), Left(AbstractRange.AbstractValue), Left(AbstractRange.AbstractValue)))
    }
    "allow to parse abstract range from valid string x.x" in {
      AbstractRange.parse("x.x") must beSome(AbstractRange(Left(AbstractRange.AbstractValue), Left(AbstractRange.AbstractValue), Left(AbstractRange.AbstractValue)))
    }
    "allow to parse abstract range from valid string 1.x.x" in {
      AbstractRange.parse("1.x") must beSome(AbstractRange(Right(1), Left(AbstractRange.AbstractValue), Left(AbstractRange.AbstractValue)))
    }
    "allow to parse abstract range from valid string 1.x.4" in {
      AbstractRange.parse("1.x.4") must beSome(AbstractRange(Right(1), Left(AbstractRange.AbstractValue), Right(4)))
    }
    "allow to parse abstract range from valid string x.x.4" in {
      AbstractRange.parse("x.x.4") must beSome(AbstractRange(Left(AbstractRange.AbstractValue), Left(AbstractRange.AbstractValue), Right(4)))
    }

    "not allow to parse invalid string 1.y" in {
      AbstractRange.parse("1.y") must beNone
    }
    "not allow to parse invalid string 1.2.3.4" in {
      AbstractRange.parse("1.2.3.4") must beNone
    }

    "allow access to major version" in {
      AbstractRange.parse("1.2.3").get.major must beRight(1)
      AbstractRange.parse("x.2.3").get.major must beLeft(AbstractRange.AbstractValue)
    }
    "allow access to minor version" in {
      AbstractRange.parse("1.2.3").get.minor must beRight(2)
      AbstractRange.parse("1.x.3").get.minor must beLeft(AbstractRange.AbstractValue)
    }
    "allow access to patch version" in {
      AbstractRange.parse("1.2.3").get.patch must beRight(3)
      AbstractRange.parse("1.2.x").get.patch must beLeft(AbstractRange.AbstractValue)
    }

    "allow to instantiate an AbstractRange" in {
      AbstractRange.parse("1.2.3") must beSome(AbstractRange(major = Right(1), minor = Right(2), patch = Right(3)))
      AbstractRange.parse("x.x.x") must beSome(AbstractRange(major = Left(AbstractRange.AbstractValue), minor = Left(AbstractRange.AbstractValue), patch = Left(AbstractRange.AbstractValue)))
    }

    "have a pretty toString" in {
      AbstractRange.parse("0.0.0").map(_.toString) must beSome("0.0.0")
      AbstractRange.parse("1.2.3").map(_.toString) must beSome("1.2.3")
      AbstractRange.parse("1.x.x").map(_.toString) must beSome("1.x.x")
    }

    "allow to instantiate SimpleRange from 2 SemVer" in {
      SimpleRange(from = SemVer(1,0,0), to = SemVer(1,2,3)) must beAnInstanceOf[SemVerRange]
    }
    "throw an exception if SimpleRange is instantiated with from > to" in {
      SimpleRange(from = SemVer(1,5,0), to = SemVer(1,2,3)) must throwA[IllegalArgumentException]
    }
  }
}