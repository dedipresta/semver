package com.dedipresta.semver

import scala.util.matching.Regex

/**
  * Object representation of semantic version label
  * according to http://semver.org/spec/v2.0.0.html
  *
  * @param major      the major version
  * @param minor      th minor version
  * @param patch      the patch version
  * @param preRelease the pre-release info
  * @param metaData   the meta data info
  */
case class SemVer(
                   major: Int = 0,
                   minor: Int = 0,
                   patch: Int = 0,
                   preRelease: Seq[SemVer.PreRelease] = Seq.empty[SemVer.PreRelease],
                   metaData: Seq[SemVer.MetaData] = Seq.empty[SemVer.MetaData]
                 ) extends Ordered[SemVer] {

  import SemVer._

  /**
    * Human readable version of the SemVer
    *
    * @return a formatted String
    */
  override def toString: String = {
    val _preRelease = if (preRelease.nonEmpty) s"""+${preRelease mkString "."}""" else ""
    val _metaData = if (metaData.nonEmpty) s"""+${metaData mkString "."}""" else ""
    s"""$major.$minor.$patch${_preRelease}${_metaData}"""
  }

  /**
    * Compare 2 SemVer with no precedence on meta data part
    *
    * @param that the instance to compare
    * @return
    */
  override def compare(that: SemVer): Int = {
    import scala.math.Ordered.orderingToOrdered
    (major, minor, patch, preRelease.mkString) compare (that.major, that.minor, that.patch, that.preRelease.mkString)
  }

  /**
    * Check if a pre-release label is defined
    *
    * @return
    */
  def isPreRelease: Boolean = preRelease.nonEmpty

  /**
    * Check if meta data are defined
    *
    * @return
    */
  def hasMetaData: Boolean = metaData.nonEmpty

  /**
    * Check if this version is unstable release
    *
    * @return
    */
  def isUnstable: Boolean = preRelease.nonEmpty

  /**
    * Check if this version is a stable release
    *
    * @return
    */
  def isStable: Boolean = preRelease.isEmpty

  /**
    * Increments major version and reset lower info
    *
    * @return updated object
    */
  def incrementMajor: SemVer = this.copy(major + 1, minor = 0, patch = 0, preRelease = Seq(), metaData = Seq())

  /**
    * Increments minor version and reset lower info
    *
    * @return updated object
    */
  def incrementMinor: SemVer = this.copy(minor = minor + 1, patch = 0, preRelease = Seq(), metaData = Seq())

  /**
    * Increments patch version and reset lower info
    *
    * @return updated object
    */
  def incrementPatch: SemVer = this.copy(patch = patch + 1, preRelease = Seq(), metaData = Seq())

  /**
    * Add pre-release info
    *
    * @return updated object
    */
  def withPreRelease(pr: Seq[PreRelease]): SemVer = {
    if (SemVer.isValidPreRelease(pr)) {
      this.copy(preRelease = pr)
    } else {
      throw new IllegalArgumentException(s"preRelease list: $pr")
    }
  }

  /**
    * Add pre-release info
    *
    * @return updated object
    */
  def withPreRelease(prString: String): SemVer = {
    val pr = prString.split("[.]")
    if (SemVer.isValidPreRelease(pr)) {
      this.copy(preRelease = pr)
    } else {
      throw new IllegalArgumentException(s"preRelease string: $prString")
    }
  }

  /**
    * Add meta-data info
    *
    * @return updated object
    */
  def withMetaData(md: Seq[MetaData]): SemVer = {
    if (SemVer.isValidMetaData(md)) {
      this.copy(metaData = md)
    } else {
      throw new IllegalArgumentException(s"metaData list: $md")
    }
  }

  /**
    * Add meta-data info
    *
    * @return updated object
    */
  def withMetaData(mdString: String): SemVer = {
    val md = mdString.split("[.]")
    if (SemVer.isValidMetaData(md)) {
      this.copy(metaData = md)
    } else {
      throw new IllegalArgumentException(s"metaData string: $mdString")
    }
  }

  /**
    * Check if current version satisfies a range
    *
    * @param range to check with
    * @return true if SemVer is in the range
    */
  def satisfies(range: SemVerRange): Boolean = range match {
    case simpleRange: SimpleRange => this >= simpleRange.from && this <= simpleRange.to
    case AbstractRange(_major, _minor, _patch) =>
      val kv = List(_major, _minor, _patch) zip List(major, minor, patch)
      kv forall { case (k, v) =>
        k match {
          case Left(_) => true
          case Right(v1) => v == v1
        }
      }
  }

  /**
    * Check if current version satisfies a range
    *
    * @param ranges set of ranges to check with
    * @return true if SemVer is in a range
    */
  def satisfies(ranges: Set[SemVerRange]): Boolean = ranges exists satisfies
}

object SemVer {

  /**
    * Alias type for MetaData type
    */
  type MetaData = String

  /**
    * Alias type for PreRelease type
    */
  type PreRelease = String

  /**
    * Pattern for semantic version label parsing
    */
  private final val mainPattern: Regex = """^(\d+)\.(\d+)\.(\d+)(-[0-9a-zA-Z.-]+)?([+][0-9a-zA-Z.-]+)?$""".r

  /**
    * Pattern to validate pre-release and meta-data parts
    */
  private final val listPattern: Regex = """^[0-9a-zA-Z.-]+$""".r

  /**
    * Parse a semantic version label to a SemVer obect
    *
    * @param label to parse
    * @return optional SemVer if parsing is successful
    */
  def parse(label: String): Option[SemVer] = label match {
    case mainPattern(major, minor, patch, nullableHyphenedPreRelease, nullablePlusAndMetaData) =>
      val _preRelease: Seq[PreRelease] = Option(nullableHyphenedPreRelease).map(_.tail.split("[.]").toList) getOrElse Nil
      val _metaData: Seq[MetaData] = Option(nullablePlusAndMetaData).map(_.tail.split("[.]").toList) getOrElse Nil

      if (isValidMajor(major) && isValidMinor(minor) && isValidPatch(patch) &&
        isValidPreRelease(_preRelease) && isValidMetaData(_metaData)) {
        Some(SemVer(major.toInt, minor.toInt, patch.toInt, _preRelease, _metaData))
      } else {
        None
      }
    case _ => None
  }

  /**
    * Check a semantic version label validity
    *
    * @param label the version text label
    * @return true if valid
    */
  def isValid(label: String): Boolean = parse(label).nonEmpty

  /**
    * Check a semantic version label invalidity
    *
    * @param label the version text label
    * @return true if invalid
    */
  def isInvalid(label: String): Boolean = !isValid(label)

  /**
    * Check major part of the label validity
    *
    * @param s the major part of the label
    * @return
    */
  private def isValidMajor(s: String): Boolean = nonLeadingZeroOrZero(s)

  /**
    * Check minor part of the label validity
    *
    * @param s the minor part of the label
    * @return
    */
  private def isValidMinor(s: String): Boolean = nonLeadingZeroOrZero(s)

  /**
    * Check patch part of the label validity
    *
    * @param s the patch part of the label
    * @return
    */
  private def isValidPatch(s: String): Boolean = nonLeadingZeroOrZero(s)

  /**
    * Check pre-release part of the label validity
    *
    * @param preReleaseSeq the pre-release part of the label
    * @return
    */
  private def isValidPreRelease(preReleaseSeq: Seq[String]): Boolean = isValidListOfInfo(preReleaseSeq)

  /**
    * Check meta-data part of the label validity
    *
    * @param metaDataSeq the meta-data part of the label
    * @return
    */
  private def isValidMetaData(metaDataSeq: Seq[MetaData]): Boolean = isValidListOfInfo(metaDataSeq)

  /**
    * Check that a String is "0" or does not start with "0"
    *
    * @param s the string to check
    * @return
    */
  private def nonLeadingZeroOrZero(s: String): Boolean = s.length == 1 || !s.headOption.contains('0')

  /**
    * Validate pre-release and meta-data list
    *
    * @param seq sequence of string to validate
    * @return
    */
  private def isValidListOfInfo(seq: Seq[String]) = seq.forall { s =>
    s.nonEmpty && nonLeadingZeroOrZero(s) &&
      !s.contains(".") &&
      s.matches(listPattern.regex)
  }

  /**
    * Get the minimum version that satisfies a range
    *
    * @param choices choices containing the minimum version
    * @param ranges  ranges to match with
    * @return the minimum if exists
    */
  def minSatisfy(choices: Set[SemVer], ranges: Set[SemVerRange]): Option[SemVer] = {
    val s = choices.filter(_.satisfies(ranges))
    if (s.nonEmpty) Some(s.min) else None
  }

  /**
    * Get the maximum version that satisfies a range
    *
    * @param choices choices containing the minimum version
    * @param ranges  ranges to match with
    * @return the maximum if exists
    */
  def maxSatisfy(choices: Set[SemVer], ranges: Set[SemVerRange]): Option[SemVer] = {
    val s = choices.filter(_.satisfies(ranges))
    if (s.nonEmpty) Some(s.max) else None
  }
}
