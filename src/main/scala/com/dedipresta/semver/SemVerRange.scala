package com.dedipresta.semver

import scala.util.Try


/**
  * Define a range of SemVer
  */
sealed trait SemVerRange

/**
  * Define a range using bounds
  *
  * @param from start SemVer
  * @param to   end SemVer
  */
case class SimpleRange(from: SemVer, to: SemVer) extends SemVerRange {
  if(from > to) throw new IllegalArgumentException("from > to")
}

/**
  * Define a range using a string with anonymous part7
  * e.g 1.x, 1.5.x, 2.x.x
  *
  * @param major major version
  * @param minor minor version
  * @param patch patch version
  */
case class AbstractRange(major: Either[Char, Int] = Left(AbstractRange.AbstractValue),
                         minor: Either[Char, Int] = Left(AbstractRange.AbstractValue),
                         patch: Either[Char, Int] = Left(AbstractRange.AbstractValue)
                        ) extends SemVerRange {

  /**
    * Stringify either from the string value of its fields
    *
    * @param e Either to stringify
    * @tparam A Left type
    * @tparam B Right type
    * @return the string value
    */
  private def eitherToString[A, B](e: Either[A, B]): String = e match {
    case Left(a) => a.toString
    case Right(b) => b.toString
  }

  /**
    * Customize string format
    *
    * @return the formatted String
    */
  override def toString: String = s"""${eitherToString(major)}.${eitherToString(minor)}.${eitherToString(patch)}"""
}

object AbstractRange {

  /**
    * Abstract value used as placeholder for range
    */
  val AbstractValue = 'x'

  /**
    * Parse a Strig to an AbstractRange
    *
    * @param s the string to parse
    * @return an AbstractRange if parsing is successful
    */
  def parse(s: String): Option[AbstractRange] = Try {
    val list = s.split("[.]").toList.map {
      case "x" => Left(AbstractValue)
      case i if i.toInt >= 0 => Right(i.toInt)
    }
    list ::: List.fill(3 - list.length)(Left(AbstractValue))
  }.toOption.flatMap {
    case List(m1, m2, p) => Some(AbstractRange(m1, m2, p))
    case _ => None
  }
}
