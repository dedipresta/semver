name := "semver"

organization := "com.dedipresta"

version := "1.1.0"

scalaVersion := "2.12.8"

crossScalaVersions := Seq("2.11.12", "2.12.8")

coverageEnabled := true


val specs2Version = "3.9.0"

// Read here for optional jars and dependencies
libraryDependencies ++= Seq(
  "org.specs2" %% "specs2-core" % specs2Version % "test",
  "org.specs2" %% "specs2-junit" % specs2Version % "test"
)

scalacOptions in Test ++= Seq("-Yrangepos")

// publish

publishTo := Some("Artifactory Realm" at "https://www.dedipresta.com/artifactory/mvn-release-oss/")

credentials += Credentials(new File("credentials.properties"))

publishMavenStyle := true
